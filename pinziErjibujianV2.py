# coding: utf-8

__author__ = 'Tonzh'
#手动选择轮廓！

import win32gui
import win32api
import pyHook
import logging
import sys
import codecs,time

logger = logging.getLogger('HOOK')
logger.setLevel(logging.DEBUG)
if not logger.handlers:
	streamer = logging.StreamHandler()
	streamer.setLevel(logging.DEBUG)
	# fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	# streamer.setFormatter(fmt)
	logger.addHandler(streamer)

# -------------------
# glyph shape
# -------------------

def GetContourBBox(g, i0, length):
	
	"""
	Get Contour BBox from g.nodes[i0] to g.nodes[i0+length]
	"""
	
	if g.bounding_box.x != 32767 and length == len(g):
		return Rect(g.bounding_box)
	
	r = Rect(g.nodes[i0].point, g.nodes[i0].point)
	
	for i in xrange(i0 + 1, i0 + length):
		p = g.nodes[i].point
		r = r + p
	
	return r


def GetBBox(glyph, i0=0, length=None, font=fl.font):
	
	"""
	recursively go through components and sum bounding box in centain scale
	"""
	
	if glyph.bounding_box.x != 32767 and length == len(g):
		return Rect(glyph.bounding_box)
	
	# font = fl.font  # current font
	box = None
	
	if glyph.nodes:
		box = GetContourBBox(glyph, i0, length if length else len(glyph))
	if glyph.components:
		for c in glyph.components:
			g = font[c.index]
			r = GetBBox(g, i0, length, font)
			r.Shift(c.delta)
			if box is not None:
				box.Include(r)
			else:
				box = r
	return box


# -------------------
# Self Methods and Attributes
# -------------------
'''
使用规则
is_shiftdown = True 鼠标点击：Get_Con_Sel
is_shiftdown = False wait_copy_mouse = True 鼠标点击：do_Copy_con

'''
is_shiftdown = False

is_updown = False
is_leftdown = False
is_rightdown = False
is_downdown = False

is_cdown = False #复制轮廓标记
is_fdown = False #均匀分布标记
is_adown = False #对齐标记

con_selected = []

wait_copy_mouse = False
wait_fangxian_down = False


do_it = False #判断可以圈划
input_file_list = [] #拼字表
output_file_list = [] #索引表
coun_input = 0 #拼字表行数
saw_input = 0 #拼字表已遍历
pt_down = 0
pt_up = 0
saw_line = 0
count_line = 0
line_info = []
Char0_Rect = 0
g = fl.glyph

class MyDialog1: #当前想要圈划的基础字
  def __init__(self,out_line):
    self.d = Dialog(self)
    self.d.size = Point(300, 80)
    self.d.Center()
    self.d.title = u2g('当前待圈选部件字')
    self.d.AddControl(STATICCONTROL, Rect(aIDENT, aIDENT, aIDENT, aIDENT), 'label1', STYLE_LABEL, out_line)   
  def on_ok(self, code):
    return 1
  def Run(self):
    return self.d.Run()

def readnewfiles(input_file_name):
	is_unicode = False
	f_in = open(input_file_name, 'r')
	input_list = []
	text = f_in.read()
	for encoding_way in ['ascii','gbk','utf-8','utf-16','utf-16-le', 'utf-16-be']:
		try:
			text = text.decode(encoding_way)
			break
		except UnicodeDecodeError:
			continue
		except:
			print '无法读取输入文件，请检查！'
			sys.exit()
	lines = text.splitlines()
	for line in lines:
		line_new = []
		line = line.replace('\r', '')
		line = line.replace('\n', '')
		line = line.replace(' ', '')
		input_list.append(line)
	return 	input_list

	
def check_point_in_Con(p, con_p_list):
	#判断点在轮廓内， con_p_list 为轮廓点列表，表尾插入第一个点
	con_p_list.append(con_p_list[0])
	temp = 0
	#print type(p)
	px = p.x
	py = p.y
	flag = False
	for i in range(len(con_p_list)-1):
		sx = con_p_list[i+1].x
		sy = con_p_list[i+1].y
		tx = con_p_list[i].x
		ty = con_p_list[i].y
		if (py == ty and px == tx) or (py == sy and px == sx):
			return True
		if (py==ty and py ==sy and px < max(sx,tx) and px > min(sx,tx)):
			return True
		if (py > sy and py <= ty) or (py <= sy and py > ty):
			x = sx + (py - sy) * (tx - sx) / (ty - sy)
			if abs(x - px) < 1:
				return True
			if x > px:
				#print x
				flag = not flag
	return flag

def Get_Con_Sel(pt): #判断鼠标点击的点是否在字体轮廓内？在，记录轮廓
	global con_selected
	
	g = fl.glyph
	nodes = g.nodes
	#print len(nodes)
	all_con_num = g.GetContoursNumber()
	for con_idx in range(all_con_num):
		start = g.GetContourBegin(con_idx)
		length = g.GetContourLength(con_idx)
		if [start, length] not in con_selected:
			con_nodes = nodes[start:start+length]
			#print start, length, len(con_nodes)
			if check_point_in_Con(pt, con_nodes):
				con_selected.append([start,length])
	#print len(con_selected)
	return 1

def get_shif_points(p1,p2):
	return Point(p1.x-p2.x, p1.y-p2.y)
	
def mid_point(node1,node2): #返回两点的中点
	return Point((node1.x+node2.x)/2.0,(node1.y+node2.y)/2.0)

	print "in do_with_Shiftup()"
	global con_selected
	global wait_copy_mouse
	global wait_fangxian_down
	global is_adown
	global is_fdown
	global is_cdown
	global g
	g = fl.glyph
	R_list = []
	len_con = len(con_selected)
	if len_con != 0:
		#标记选中的轮廓点为选中态
		for item in con_selected:
			for i in range(item[0], item[0]+item[1]):
				g.nodes[i].selected = 1			
		fl.UpdateFont()
		if is_adown or is_fdown:
			wait_fangxian_down = True
		if is_cdown:
			#print 'c down'
			wait_copy_mouse = True
			#print "wait_copy_mouse", wait_copy_mouse


	print "in do_Copy_con()"
	
	g = fl.glyph
	R_list = []
	for item in con_selected:
		R_list.append(GetContourBBox(g, item[0], item[1]))		
	r = R_list[0]
	for i in R_list:
		r = r + i
	center = mid_point(r.ll,r.ur)
	#print pt,center
	shif_point = get_shif_points(pt,center)
	#print shif_point
	tg = Glyph()
	for item in con_selected:
		g0 = Glyph(1, g.nodes[item[0]:item[0]+item[1]])
		g0.Shift(shif_point)
		tg.Insert(g0)
	g.Insert(tg)
	fl.UpdateFont()
	
	is_updown = False
	is_leftdown = False
	is_rightdown = False
	is_downdown = False
	is_cdown = False #复制轮廓标记
	is_fdown = False #均匀分布标记
	is_adown = False #对齐标记
	con_selected = []
	wait_copy_mouse = False
	wait_fangxian_down = False
	return 1

def do_get_info(): #鼠标抬起，获取圈划rect，处理信息
	global do_it #判断可以圈划
	global input_file_list #拼字表
	global output_file_list #索引表
	global coun_input #拼字表行数
	global saw_input #拼字表已遍历
	global pt_down
	global pt_up
	global saw_line
	global count_line
	global line_info
	global Char0_Rect
	
	glyphs = fl.font.glyphs
	r0 = Rect(pt_down)
	r0 +=pt_up
	#print r0
	
	line = input_file_list[saw_input]
	line0 = line.strip()
	char = line0[saw_line]
	
	g_idx = fl[0].FindGlyph(ord(char))
	if g_idx!=-1:
		Conidxs = []
		g = glyphs[g_idx]
		g.SelectRect(r0)
		nodes = g.nodes
		nodes_sel = g.Selection()
		for i in range(len(nodes_sel)):
			for j in range(len(nodes)):
				if nodes_sel[i].point == nodes[j].point:
					conidx = g.FindContour(j)  
					if conidx not in Conidxs:
						Conidxs.append(conidx)
					break
		g.UnselectAll()
		g_r = Rect(r0)
		for idx in Conidxs:
			i0 = g.GetContourBegin(idx)
			length = g.GetContourLength(idx)
			g_r += GetContourBBox(g, i0, length)
		if saw_line == 0:
			Char0_Rect = g_r
		center = Point((g_r.ll.x+g_r.ur.x)/2,(g_r.ll.y+g_r.ur.y)/2)
		Scale = Point(float(g_r.width)/Char0_Rect.width,float(g_r.height)/Char0_Rect.height)
		if saw_line == 0:
			line_info.append([g.unicode,r0,center,Scale])
		else:
			line_info.append([g.unicode,center,Scale])
	saw_line += 1
	if saw_line == count_line:
		saw_line = 0
		saw_input += 1
		output_file_list.append(line_info)
		line_info = []
		if len(output_file_list) > 50:
			Write_Info()
	if saw_input<coun_input:
		line = input_file_list[saw_input]
		line0 = line.strip()
		g_idx = fl[0].FindGlyph(ord(line0[saw_line]))
		if g_idx!=-1:
			fl.iglyph = g_idx
		if saw_line == 0:
			count_line = len(line)
			result_str = line.encode('GBK')
			print >>sys.stderr, result_str
			#d = MyDialog1(result_str)
			#if d.Run() != 0:
			#	do_it = True
			
def is_Bdown(): #键盘“B”或“b”,back操作！！
	global output_file_list #索引表
	global saw_input #拼字表已遍历
	global saw_line
	global count_line
	global line_info
	
	if saw_line == 0:
		line_info = output_file_list.pop()
		saw_input -= 1
		line_info.pop()
		saw_line = len(line_info)
	else:
		saw_line -= 1
		line_info.pop()
	line = input_file_list[saw_input]
	line0 = line.strip()
	g_idx = fl[0].FindGlyph(ord(line0[saw_line]))
	if g_idx!=-1:
		fl.iglyph = g_idx
		count_line = len(line)
		result_str = line.encode('GBK')
		print >>sys.stderr, result_str
# -------------------
# Hook Method
# -------------------

hm = None

def Write_Info():
	global output_file_list #索引表
	f_out = codecs.open('PinziOutPut.txt', 'a')
	#print output_file_list
	for line_info in output_file_list:
		item0 = line_info[0]
		str_uni = hex(item0[0])+'*'
		str_r0 = str(item0[1].ur.x)+','+str(item0[1].ur.y)+','+str(item0[1].ll.x)+','+str(item0[1].ll.y)
		str_info =str_uni+str_r0
		for info in line_info[1:]:
			str_uni = ' '+hex(info[0])
			str_center = '*'+str(info[1].x)+','+str(info[1].y)
			str_shift =  '*'+str(info[2].x)+','+str(info[2].y)
			str_info += str_uni+str_center+str_shift
		f_out.write(str_info+'\r\n')
		print str_info
	f_out.close()

def onExit():
	global hm
	global do_it #判断可以圈划
	global input_file_list #拼字表
	global output_file_list #索引表
	global coun_input #拼字表行数
	global saw_input #拼字表已遍历
	global pt_down
	global pt_up
	hm.UnhookMouse()
	hm.UnhookKeyboard()
	del hm
	logger.info('... Exit.')
	#print output_file_list
	Write_Info()

def onMouseAll(event):
	global is_shiftdown
	global is_updown
	global is_leftdown
	global is_rightdown
	global is_downdown
	global is_cdown #复制轮廓标记
	global is_fdown #均匀分布标记
	global is_adown #对齐标记
	global con_selected
	
	global do_it #判断可以圈划
	global input_file_list #拼字表
	global output_file_list #索引表
	global coun_input #拼字表行数
	global saw_input #拼字表已遍历
	global pt_down
	global pt_up
	# logger.debug("MessageName: {}".format(event.MessageName))
	# logger.debug("Message: {}".format(event.Message))
	# logger.debug("Time: {}".format(event.Time))
	# logger.debug("Window: {}".format(event.Window))
	# logger.debug("WindowName: {}".format(event.WindowName))
	# logger.debug("Position: {}".format(event.Position))
	# logger.debug("Wheel: {}".format(event.Wheel))
	# logger.debug("Injected: {}".format(event.Injected))
	# logger.debug("---")
	# find FontLab Glyph window
	w = event.Window
	title = win32gui.GetWindowText(fl.window)
	if title and title.startswith('Glyph - ') and win32gui.IsChild(fl.window, w):
		msg = event.MessageName.lower()
		wp = win32gui.ScreenToClient(w, event.Position)
		pt = fl.ScreenToGlyph(Point(*wp))
		#print 'hit pt test:', fl.HitContour(pt), pt
		if 'mouse left down' == msg and do_it:
			pt_down = pt
		if 'mouse left up' == msg and do_it:
			
			pt_up = pt
			do_get_info()
			#print coun_input, saw_input
			if coun_input == saw_input:
				onExit()
			
	return True
	

def onKeyboardEvent(event):
	global is_shiftdown
	global is_updown
	global is_leftdown
	global is_rightdown
	global is_downdown
	global is_cdown #复制轮廓标记
	global is_fdown #均匀分布标记
	global is_adown #对齐标记
	
	global wait_fangxian_down
	# logger.debug("MessageName: {}".format(event.MessageName))
	# logger.debug("Message: {}".format(event.Message))
	# logger.debug("Time: {}".format(event.Time))
	# logger.debug("Window: {}".format(event.Window))
	# logger.debug("WindowName: {}".format(event.WindowName))
	# logger.debug("Ascii: {} {}".format(event.Ascii, chr(event.Ascii)))
	# logger.debug("Key: {}".format(event.Key))
	# logger.debug("KeyID: {}".format(event.KeyID))
	# logger.debug("ScanCode: {}".format(event.ScanCode))
	# logger.debug("Extended: {}".format(event.Extended))
	# logger.debug("Injected: {}".format(event.Injected))
	# logger.debug("Alt {}".format(event.Alt))
	# logger.debug("Transition {}".format(event.Transition))
	# logger.debug("---")
	w_active = win32gui.GetActiveWindow()
	w = event.Window
	if fl.mainwindow == w_active or win32gui.IsChild(fl.mainwindow, w_active)\
	or win32gui.IsChild(fl.window, w):
		if event.Ascii == 27:
			onExit()
			return True
	title = win32gui.GetWindowText(fl.window)  # navigation by key '<' or '>'
	if title and title.startswith('Glyph - ') and (fl.mainwindow == w_active or win32gui.IsChild(fl.mainwindow, w_active)\
	or win32gui.IsChild(fl.window, w)):
		key = event.Key
		
		is_keydown = bool(event.MessageName == 'key down')
		#logger.debug(key)
		#print type(key)
		#print key,is_keydown
		if key.find('shift') >= 0 and is_keydown:
			is_shiftdown = True
			#logger.debug('shift down')
			return True
		if key.find('shift') >= 0 and not is_keydown:
			is_shiftdown = False
			#do_with_Shiftup()
			#logger.debug('shift up')
			return True
		if key == 'control' and is_keydown:
			#logger.debug('control key: %s',key)
			return True
		#print event.KeyID == 38 , is_keydown , wait_fangxian_down
		
		
		if (event.Ascii == 0x62 or event.Ascii == 0x42) and is_keydown:
			is_Bdown()
			#logger.debug('a down')
	#print is_adown, is_fdown

	return True

def init_hook():
	global hm
	
	hm = pyHook.HookManager()
	hm.MouseAll = onMouseAll
	hm.HookMouse()
	hm.KeyAll = onKeyboardEvent
	hm.HookKeyboard()
	logger.info('Hook from now on')
	# Don't bother to call PumpMessages because GUI programs owning one
	
def u2g(string):
	return string.decode('utf-8').encode('gbk')


def main():
	global do_it
	global input_file_list
	global coun_input
	global count_line
	
	flsys.fl_quit = BaseException('Stop')
	def my_trace(frame, event, arg):
		return my_trace
	flsys.my_trace = my_trace
	#启动监听程序
	init_hook()
	
	#初始化拼字表
	input_file_name = 'pinzibiao.txt'
	input_file_list = readnewfiles(input_file_name)
	coun_input = len(input_file_list)
	if coun_input == 0:
		print "Wrong TXT!"
		return 0
	print "There are: ",coun_input,' lines in the TXT!'
	for line in input_file_list:
		line0 = line.strip()
		g_idx = fl[0].FindGlyph(ord(line0[0]))
		if g_idx!=-1:
			fl.iglyph = g_idx
			do_it = True
			#print line
			result_str = line.encode('GBK')
			print >>sys.stderr, result_str
			count_line = len(line)
			break
		else:
			print hex(ord(line0[0]))+' not in font!'
	

if __name__ == "__main__":
	main()
