# coding: utf-8

__author__ = 'Tonzh'

import win32gui
import win32api
import pyHook
import logging
import sys
import codecs,time
	

#读取拼字文件
f_in = open('PinziOutPut.txt', 'r')
text = f_in.read()
for encoding_way in ['ascii','gbk','utf-8','utf-16','utf-16-le', 'utf-16-be']:
	try:
		text = text.decode(encoding_way)
		break
	except UnicodeDecodeError:
		continue
	except:
		print '无法读取输入文件，请检查！'
		sys.exit()
lines = text.splitlines()
f_in.close()
glyphs_New = fl[1].glyphs #薪资库
glyphs = fl[0].glyphs


for line in lines:
	line = line.strip()
	lines = line.split()
	if len(lines)>1:
		#解析首调base信息
		base_items = lines[0].split('*')
		#基础字uni
		Base_uni = int(base_items[0].replace('0x',''),16)
		r0xs = base_items[1].split(',')
		#print r0xs
		p1 = Point(float(r0xs[0]),float(r0xs[1]))
		p2 = Point(float(r0xs[2]),float(r0xs[3]))
		#圈划Rect
		Base_r0 = Rect(p1)
		Base_r0 += p2
		base_idx = fl[0].FindGlyph(Base_uni)
		if base_idx!=-1:
			Conidxs = []
			base_glyph =  glyphs[base_idx]
			#创建部件模板
			base_glyph.SelectRect(Base_r0)
			nodes = base_glyph.nodes
			nodes_sel = base_glyph.Selection()
			for i in range(len(nodes_sel)):
				for j in range(len(nodes)):
					if nodes_sel[i].point == nodes[j].point:
						conidx = base_glyph.FindContour(j)  
						if conidx not in Conidxs:
							Conidxs.append(conidx)
						break
			Base_nodes = []
			for idx in Conidxs:
				i0 = base_glyph.GetContourBegin(idx)
				length = base_glyph.GetContourLength(idx)
				Base_nodes.extend(nodes[i0:i0+length])
			New_base_g = Glyph()
			New_base_g.Insert(Base_nodes)
			New_base_g.Shift(Point(-(Base_r0.ll.x+Base_r0.ur.x)/2,-(Base_r0.ll.y+Base_r0.ur.y)/2))
			New_base_g.width = advance_width = 1000
			
			#逐条解析拼字信心，并处理
			for info in lines[1:]:
				items = info.split('*')
				New_uni = int(items[0].replace('0x',''),16)
				cenxs = items[1].split(',')
				Center = Point(float(cenxs[0]),float(cenxs[1]))
				sftxs = items[2].split(',')
				Scale = Point(float(sftxs[0]),float(sftxs[1]))
				
				#处理拼字
				print hex(Base_uni),Base_r0,hex(New_uni),Center,Scale
				temp_g = Glyph(New_base_g)
				temp_g.Scale(Scale)
				temp_g.Shift(Center)
				Pin_idx = fl[1].FindGlyph(New_uni)
				
				if Pin_idx!=-1:
					glyphs_New[Pin_idx].Insert(temp_g)
				else:
					temp_g.unicode = New_uni
					glyphs_New.append(temp_g)
				fl.UpdateFont(1)
			
			
